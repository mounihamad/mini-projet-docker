# MINI-DOCKER-PROJECT : student-list

In this projet we have to deploy an application named "student_list", which is very basic and enables POZOS to show the list of some students with their age.

## About the owner

- First Name : MOHAMADOU
- Last Name : **Mounirou**
- Session : Bootcamp DevOps N°15
- Period : September - November 2023
- [![linkedin](https://img.shields.io/badge/linkedin-0A66C2?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/mohamadou-mounirou-816150126/)

## Module

This application has two modules:

- The first module is a REST API (with basic authentication needed) who send the desire list of the student based on JSON file
- The second module is a web app written in HTML + PHP who enable end-user to get a list of students

## The need

In this work we have to :

1. build one container for each module
2. make them interact with each other
3. provide a private registry

## Work plan

First, i will introduce you the six **_files_** of this project and their role

Then, I'll show you how I **_built_** and tested the architecture to justify my choices

Third and last part will be about to provide the **_deployment_** process I suggest for this application.

### The files' role

In my delivery you can find three main files : a **_Dockerfile_**, a **_docker-compose.yml_** and a **_docker-compose.registry.yml_**

- docker-compose.yml: to launch the application (API and web app)
- docker-compose.registry.yml: to launch the local registry and its frontend
- simple_api/student_age.py: contains the source code of the API in python
- simple_api/Dockerfile: to build the API image with the source code in it
- simple_api/student_age.json: contains student name with age on JSON format
- index.php: PHP  page where end-user will be connected to interact with the service to list students with their age.

## Build and test

Considering you just have cloned this repository, you have to follow those steps to get the 'student_list' application ready :

1. Change directory and build the api container image :

```bash
#Enter the directory
cd ./mini-projet-docker/simple_api

#Buil the image
docker build -t api_student_list .

#Verify that the image is built and present localy
docker images
```

> ![01- docker images](https://gitlab.com/mounihamad/mini-projet-docker/uploads/adfb9892b3fe7f84e050ef0de9fccace/01.png)


**NB:** api_student_list : is the name of the image build, this name will be used in the next stape, so you can change this one as you wish.

2. Create a bridge-type network for the two containers to be able to contact each other by their names thanks to dns functions :

```bash
docker network create student_list_network --driver=bridge
docker network ls
```

> ![02- docker network ls](https://gitlab.com/mounihamad/mini-projet-docker/uploads/172b8fbdd44abfcb5330db63e92ce3e7/02.png)

3. Move back to the root dir of the project and run the backend api container with those arguments :

```bash
docker run --rm -d --name=api_student_list --network=student_list_network -v ./simple_api/:/data/ api_student_list
docker ps
```

> ![03-docker ps](https://gitlab.com/mounihamad/mini-projet-docker/uploads/4aeb3b04ad58aaaea8e7927e5250331a/03.png)

As you can see, the api backend container is listening to the 5000 port.
This internal port can be reached by another container from the same network so I chose not to expose it.

I also had to mount the `./simple_api/` local directory in the `/data/` internal container directory so the api can use the `student_age.json` list

> ![04-./simple_api/:/data/](https://gitlab.com/mounihamad/mini-projet-docker/uploads/4abb7e6cfe389dc78c449ac7ca6ada6f/app.png)

4. Update the `index.php` file :

You need to update the following line before running the website container to make **_api_ip_or_name_** and **_port_** fit your deployment
` $url = 'http://<api_ip_or_name:port>/pozos/api/v1.0/get_student_ages';`

Thanks to our bridge-type network's dns functions, we can easyly use the api container name with the port we saw just before to adapt our website

```bash
sed -i 's/<api_ip_or_name:port>/api_student_list:5000/g' ./website/index.php
```

> ![05-api.student_list:5000](https://gitlab.com/mounihamad/mini-projet-docker/uploads/95f2b52df2cd1e0f5b916b5bc10f0681/download.jpg)

5. Run the frontend webapp container :

Username and password are provided in the source code `.simple_api/student_age.py`

> ![06-id/passwd](https://gitlab.com/mounihamad/mini-projet-docker/uploads/a8967e696615761642e47434bc4fb1b8/05.png)

```bash
docker run --rm -d --name=webapp_student_list -p 80:80 --network=student_list_network -v ./website/:/var/www/html -e USERNAME=username -e PASSWORD=python php:apache
docker ps
```

> ![07-docker ps](https://gitlab.com/mounihamad/mini-projet-docker/uploads/05a72a21c77b2b11783c5cb966b60658/webapp.png)

6. Test the api through the frontend :

6a) Using command line :

The next command will ask the frontend container to request the backend api and show you the output back.
The goal is to test both if the api works and if frontend can get the student list from it.

```bash
docker exec webapp_student_list curl -u username:python -X GET http://api_student_list:5000/pozos/api/v1.0/get_student_ages
```

> ![8-docker exec](https://gitlab.com/mounihamad/mini-projet-docker/uploads/bfcbcd00d823d2078b6972ee549cb316/t2.png)

6b) Using a web browser `IP:80` :

- If you're running the app into a remote server or a virtual machine (e.g provisionned by eazytraining's vagrant file), please find your ip address typing `hostname -I`

  > ![9-hostname -I](https://user-images.githubusercontent.com/101605739/224594393-841a5544-7914-4b4f-91fd-90ce23200156.jpg)

- If not, type in your web brower `localhost:80`

Click on the List student button

> ![10-check webpage](https://gitlab.com/mounihamad/mini-projet-docker/uploads/67a03b197858df7273011d80649c6328/t3.png)

7. Clean the workspace :

Thanks to the `--rm` argument we used while starting our containers, they will be removed as they stop.
Remove the network previously created.

```bash
docker stop api_student_list
docker stop webapp_student_list
docker network rm student_list_network
docker network ls
docker ps
```

> ![11-clean-up](https://gitlab.com/mounihamad/mini-projet-docker/uploads/5d7c2f918651ab0831fd5476c5e6a74f/check.png)

## Deployment

As the tests passed we can now 'composerize' our infrastructure by putting the `docker run` parameters into a `docker-compose.yml` code.

1. Run the application (api + webapp) :

As we've already created the application image, now you just have to run :

```bash
docker-compose up -d
```

Docker-compose permits to chose which container must start first.
The api container will be first as I specified that the webapp `depends_on:` it.

> ![12-depends on](https://gitlab.com/mounihamad/mini-projet-docker/uploads/f74981da145359cef23986b1c450afca/start.png)

And the application works :

> ![13-check app](https://gitlab.com/mounihamad/mini-projet-docker/uploads/21fceaf0d036a25850281c8b3cdad6f4/i01.png)

2. Create a registry and its frontend

I used `registry:2` image for the registry, and `joxit/docker-registry-ui:static` for its frontend gui and passed some environment variables :

> ![14-gui registry env var](https://gitlab.com/mounihamad/mini-projet-docker/uploads/bf4155ff38aeb1055dfd568ec4ecb833/i03.png)

E.g we'll be able to delete images from the registry via the gui.

```bash
docker-compose -f docker-compose-registry.yml up -d
```
> ![15-cocker compose whith registry](https://gitlab.com/mounihamad/mini-projet-docker/uploads/31e8593e20cba6f9a25f32f52cb6da39/i02.png)

3. Push an image on the registry and test the gui

You have to rename it before (`:latest` is optional) :

```bash
docker image tag api_student_list:latest localhost:5000/api_student_list:latest
docker images
```

> ![17- tag the image](https://gitlab.com/mounihamad/mini-projet-docker/uploads/37cc459d6827d2be359d66aafae887da/tag1.png)

then push the new image in the registry whith this command:

```bash
docker image push localhost:5000/api_student_list:latest
```

> ![18-push image to registry](https://gitlab.com/mounihamad/mini-projet-docker/uploads/a406652c53773b8e883903c4571afd84/i05.png)

> ![19-full reg](https://gitlab.com/mounihamad/mini-projet-docker/uploads/32b4da709afa4ed95007a4b463faccab/i06.png)

> ![20-full reg details](https://gitlab.com/mounihamad/mini-projet-docker/uploads/203f85e8e93cd7619d48784e798810f5/i07.png)

---

# This concludes my Docker mini-project run report.

Throughout this project, I had the opportunity to create a custom Docker image, configure networks and volumes, and deploy applications using docker-compose. Overall, this project has been a rewarding experience that has allowed me to strengthen my technical skills and gain a better understanding of microservices principles. I am now better equipped to tackle similar projects in the future and contribute to improving containerization and deployment processes within my team and organization.

